#ifndef MY_STRING_H
#define MY_STRING_H

#include <string>

class myString
{
public:
	void myString::init(std::string str);
	std::string myString::getStr();
	unsigned int myString::getLength();
	char myString::getCharUsingSquareBrackets(unsigned int index);
	char myString::getCharUsingRoundBrackets(unsigned int index);
	char myString::getFirstChar();
	char myString::getLastChar();
	int myString::getCharFirstOccurrenceInd(char c);
	int myString::getCharLastOccurrenceInd(char c);
	int myString::gstStringFirstOccurrenceInd(std::string str);
	bool myString::isEqual(std::string s);

private:
	std::string* _ptrStr;

};

#endif MY_STRING_H