#include <iostream>
#include "Vector.h"

/*
Constructor
Input: None
Output: None
*/
Vector::Vector(int n)
{
	if (n < 2)
		n = 2;
	this->_elements = new int[n];
	this->_size = 0;
	this->_capacity = n;
	this->_resizeFactor = n;
}

/*
Destructor
Input: None
Output: None
*/
Vector::~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
		this->_elements = NULL;
	}
}

/*
The function return size of vector
Input: None
Output: None
*/
int Vector::size() const
{
	return this->_size;
}

/*
The function return capacity of vector
Input: None
Output: None
*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*
The function return vector's resizeFactor
Input: None
Output: None
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*
The function returns true if size = 0
Input: None
Output: None
*/
bool Vector::empty() const
{
	if (this->_size == 0)
		return true;
	return false;
}

//B
//Modifiers

/*
The function adds element at the end of the vector
Input: refernce int to a value to push
Output: None
*/
void Vector::push_back(const int& val)
{
	if (this->_size < this->_capacity)
		this->_elements[this->_size] = val;
	else
		/*
		If _size => _capacity:
		We will build a new array, copy the current array to the new one
		And add the new element
		*/
	{
		int* elements = new int[this->_size + this->_resizeFactor];
		for (int i = 0; i < this->_size; i++)
		{
			elements[i] = this->_elements[i];
		}
		elements[this->_size] = this->_elements[this->_size];
		delete[] this->_elements;
		this->_elements = elements;
		this->_capacity = this->_size + this->_resizeFactor;
	}
	this->_size += 1;
}

/*
The function removes and returns the last element of the vector
Input: None
Output: int - the last element of the vector
*/
int Vector::pop_back()
{
	if (this->_size == 0)
	{
		std::cout << "Error: pop from empty vector" << std::endl;
		return -9999;
	}
	int element = this->_elements[this->_size - 1];
	this->_elements[this->_size - 1] = NULL;
	return element;
}

/*
The function change the capacity
Input: int n - the wanted size of the Vector
Output: None
*/
void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		//i starts at 2 because n * 0 = 0 and n * 1 = n, which means - both cases are not included in our condition
		for (int i = 2; i < 20; i++)
		{
			if (this->_capacity + this->_resizeFactor * i >= n)
				break;
		}
		this->_capacity += this->_resizeFactor * n;
	}
}

/*
The function change _size to n, unless n is greater than the vector's capacity
Input: int n - the wanted size of the array
Output: None
*/
void Vector::resize(int n)
{
	if (n <= this->_capacity)
	{
		this->_size = n;
	}
	else if (n > this->_capacity)
		reserve(n);
}

/*
The function assigns val to all elemnts
Input: int val - The value to put in the free cells
Output: None
*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

/*
The function is doing the same as above, if new elements added their value is val
Input: int n - the wanted size of the array (extention), int& val - the wanted value to put in every new element added
Output: None
*/
void Vector::resize(int n, const int& val)
{
	resize(n);
	assign(val);
}

//C
//The big three (d'tor is above)

/*
Copy Constructor
Input: reference to other const vector
Output: None
*/
Vector::Vector(const Vector& other)
{
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
}

/*
Operator =
Input: 
Output: reference to the vector
*/
Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements;
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	return *this;
}

//D
//Element Access

/*
Operator[] - returns a referenve to the n'th element
Input: Index of the value we want to get
Output: If n < size - the value in the index, else the first element
*/
int& Vector::operator[](int n) const
{
	if (n < this->_size)
		return this->_elements[n];
	std::cout << "The index is bigger then the size" << std::endl;
	return this->_elements[0];
}