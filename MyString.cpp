#include "MyString.h"
/*
func will init a myString obj
in: string for the strPtr to point to
out: None
*/
void myString::init(std::string str)
{
	this->_ptrStr = new std::string;
	*this->_ptrStr = str;
}

/*
func will return the string of the myString
in: None
out: string in mystring
*/
std::string myString::getStr()
{
	return *(this->_ptrStr);
}


/*
func will return the length of the string of myString
in: None
out: unsigned int length
*/
unsigned int myString::getLength()
{
	return this->getStr().length();
}


/*
func will return the char at index index  in the string of the myString(use SquareBrackets)
in: index
out: char in index
*/
char myString::getCharUsingSquareBrackets(unsigned int index)
{
	return this->getStr()[index];
}


/*
func will return the char at index index  in the string of the myString(use RoundBrackets)
in: index
out: char in index
*/
char myString::getCharUsingRoundBrackets(unsigned int index)
{
	return *(this->getStr().data() + index);
}

/*
func will return the first char of the string of the myString
in: None
out: first char
*/
char myString::getFirstChar()
{
	return this->getCharUsingSquareBrackets(0);
}

/*
func will return the last char of the string of the myString
in: None
out: last char
*/
char myString::getLastChar()
{
	return this->getCharUsingSquareBrackets(this->getLength() - 1);
}

/*
func will return the first occurnce of char c in the string of the myString
in: None
out: index of the first occurnce of char c
*/
int myString::getCharFirstOccurrenceInd(char c)
{
	return this->getStr().find_first_of(c);
}

/*
func will return the last occurnce of char c in the string of the myString
in: char c
out: index of the last occurnce of char c
*/
int myString::getCharLastOccurrenceInd(char c)
{
	return this->getStr().find_last_of(c);
}

/*
func will return the first occurnce of str str in the string of the myString
in: std::string str
out: index of the first occurnce of str in the string
*/
int myString::gstStringFirstOccurrenceInd(std::string str)
{
	return this->getStr().find_first_of(str);
}


/*
func will return if string str is equal to the string of the myString
in: std::string s
out: bool if they are equal
*/
bool myString::isEqual(std::string s)
{
	return (this->getStr().compare(s) == 0);
}